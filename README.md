# http-ls

Lists files on a system via http.

## Getting started/TLDR

To run the service locally from source

```bash
go run main.go
```

To build and run the service using docker

```bash
docker build -t http-ls:latest .
docker run -p 8080:8080 -it http-ls:latest
```

To use the service in a browser or another terminal

```bash
curl localhost:8080/files/
```

## Usage:

### List the entries in a directory

#### Request

```http
http://localhost:8080/files/<path_to_search>?limit=<optional_pagination_limit>&offset<optional_pagination_offset>
```

#### Response

```json
{
  "entries": [
    {
      "path": "/.dockerenv",
      "size": 0,
      "file_mode": "-rwxr-xr-x",
      "last_modified": "2021-03-02T20:46:38.83Z",
      "is_directory": false
    },
    {
      "path": "/bin",
      "size": 4096,
      "file_mode": "drwxr-xr-x",
      "last_modified": "2021-02-17T20:07:33Z",
      "is_directory": true
    },
    {
      "path": "/dev",
      "size": 360,
      "file_mode": "drwxr-xr-x",
      "last_modified": "2021-03-02T20:46:39.16Z",
      "is_directory": true
    },
    {
      "path": "/etc",
      "size": 4096,
      "file_mode": "drwxr-xr-x",
      "last_modified": "2021-03-02T20:46:38.83Z",
      "is_directory": true
    },
    ...<omitted entries>
  ],
  "count": 18
}
```

Pagination is done via optional URL query arguments. `limit` specifies the max number of entries to
return. `offset` specifies the offset to apply to returned entries.

> `count` shows the total number of entries without considering pagination arguments

### Check service health

#### Request

```
http://localhost:8080/health
```

#### Response

```
Running since 2021-03-02 20:04:09.6183361 +0200 SAST m=+0.000350101.
Uptime 2.0649792s
```

## Notes

Listing a large number of files over http could become a slow. Pagination was added to help mitigate
this problem.

### Possible improvements

- Make use of a library that has log levels, to enable separate verbose logging of requests and
  responses.
- Make the http port configurable via command line argument.
- Add more info to the health check e.g.
  - number of queries received
  - number of errors
  - average response time
- Add monitoring that can check error rates, on an operations level. This can be done by monitoring the logs.

### Possible issues

- Listing all the files in a container might not be the safest option, depending on where this is
  meant to run.
- The service is running over http. An API gateway of some kind should be used to control and secure
  access.
