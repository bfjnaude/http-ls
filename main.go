package main

// HTTP server that will list files for a given subdirectory under GET http://example.com/files/
// Health checks can be performed at GET http://example.com/health/
// Listens on port 8080

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"time"
)

// fileEntry is a helper to print files with json tags
type fileEntry struct {
	Path         string    `json:"path"`
	Size         int64     `json:"size"`
	FileMode     string    `json:"file_mode"`
	LastModified time.Time `json:"last_modified"`
	IsDirectory  bool      `json:"is_directory"`
}

type entryResponse struct {
	Entries []fileEntry `json:"entries"`
	Count   int         `json:"count"`
}

// fileHandler handles requests for listing files in a directory
func filesHandler(w http.ResponseWriter, req *http.Request) {
	// Check for GET only requests
	if req.Method != http.MethodGet {
		http.Error(w, fmt.Sprintf("Method %s not supported", req.Method), http.StatusBadRequest)
		return
	}

	// Retrieve URL query parameters for pagination
	queryParams := req.URL.Query()
	limit := 0
	if params, present := queryParams["limit"]; present {
		if len(params) > 0 {
			limitString := params[0]
			var err error
			limit, err = strconv.Atoi(limitString)
			if err != nil {
				http.Error(w, "Could not convert limit parameter: "+err.Error(), http.StatusBadRequest)
				return
			}
		}
	}

	offset := 0
	if params, present := queryParams["offset"]; present {
		if len(params) > 0 {
			offsetString := params[0]
			var err error
			offset, err = strconv.Atoi(offsetString)
			if err != nil {
				http.Error(w, "Could not convert offset parameter: "+err.Error(), http.StatusBadRequest)
				return
			}
		}
	}

	// Get the path to list
	path := strings.TrimPrefix(req.URL.Path, "/files")
	// Make sure that we have a OS specific path separator for printing paths
	if path[len(path)-1] != os.PathSeparator {
		path = path + string(os.PathSeparator)
	}
	// Read all the files in a directory
	files, err := ioutil.ReadDir(path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	numFiles := len(files)
	totalFiles := numFiles

	if len(files) == 0 {
		fmt.Fprintf(w, "Empty directory")
		return
	}

	// If a pagination offset is defined, discard entries before the offset
	if offset > 0 {
		files = files[offset:numFiles]
		numFiles = len(files)
	}

	// If a pagination limit is defined, discard entries over the limit
	if limit > 0 && numFiles > limit {
		files = files[0:limit]
		numFiles = len(files)
	}

	// Convert file list entries to a response
	entries := make([]fileEntry, 0)
	for _, f := range files {
		entry := fileEntry{
			Path:         path + f.Name(),
			Size:         f.Size(),
			FileMode:     f.Mode().String(),
			LastModified: f.ModTime(),
			IsDirectory:  f.IsDir(),
		}
		entries = append(entries, entry)
	}

	response := entryResponse{
		Entries: entries,
		Count:   totalFiles,
	}

	// Convert response to json
	b, err := json.Marshal(response)
	if err != nil {
		http.Error(w, fmt.Sprintf("Could not list files for %s. Error message %s", path, err.Error()), http.StatusInternalServerError)
		return
	}

	w.Write(b)
}

// Basic handler to give an OK signal for health checks
func healthHandler(w http.ResponseWriter, req *http.Request) {
	t := time.Now()
	uptime := t.Sub(startTime)
	fmt.Fprintf(w, "Running since: %s. \nUptime: %s", startTime.String(), uptime.String())
}

var startTime time.Time

func init() {
	startTime = time.Now()
}

func main() {
	// Setup handler multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/files/", filesHandler)
	mux.HandleFunc("/health", healthHandler)

	// Create server and listen in separate go routine
	server := &http.Server{Addr: ":8080", Handler: mux}
	go func() {
		log.Println("Start listening on port 8080")
		if err := server.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	// Create a channel to listen for operating system events
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	// Wait for a terminate event before killing the http server
	<-stop
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	log.Println("Received terminate signal from OS. Stopping http server")

	if err := server.Shutdown(ctx); err != nil {
		log.Println(err)
	}
}
