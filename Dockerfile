## Build using golang container 
FROM golang:1.16-buster AS builder
WORKDIR /go/src/gitlab.com/http-ls 
COPY . .
RUN GOOS=linux CGO_ENABLED=0 go build -o app .

## Run binaries in alpine
FROM alpine:latest AS bin
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/http-ls/app . 
CMD ["./app"]
EXPOSE 8080